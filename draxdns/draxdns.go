package draxdns

import (
	"context"
	"fmt"
	"net"
	"sync"

	"github.com/coredns/coredns/plugin"
	"github.com/coredns/coredns/plugin/pkg/fall"
	"github.com/coredns/coredns/request"

	"github.com/miekg/dns"
)

type DraxDNSOptions struct {
	//Time to live, for the DNS resolution response
	ttl uint32
}

//DraxDNS is the plugin handler
type DraxDNS struct {
	Next plugin.Handler
	Fall fall.F
	DraxDNSOptions
	*Fetcher
}

// ServeDNS implements the plugin.Handle interface.
func (h DraxDNS) ServeDNS(ctx context.Context, w dns.ResponseWriter, r *dns.Msg) (retInt int, retErr error) {
	state := request.Request{W: w, Req: r}
	qname := state.Name()

	log.Debug("drax-dns going to ServeDNS for ", qname, " ", state)

	var lazyMustFetchDestinations func() Destinations
	{
		var once sync.Once
		var dests Destinations
		var err error
		lazyMustFetchDestinations = func() Destinations {
			once.Do(func() {
				dests, err = h.fetchDestinations(qname)
			})
			if err != nil {
				errorStr := fmt.Sprintf("Could not fetch destinations for zone %s, error: %s", qname, err)
				retErr = err
				panic(errorStr)
			}
			return dests
		}
	}

	func() {
		answers := []dns.RR{}

		defer func() {
			if recovered := recover(); recovered != nil {
				log.Error(recovered)
				retInt = dns.RcodeServerFailure
			}
		}()
		switch state.QType() {
		case dns.TypeA:
			ips := lazyMustFetchDestinations().ipv4
			answers = a(qname, h.ttl, ips)
		case dns.TypeAAAA:
			ips := lazyMustFetchDestinations().ipv6
			answers = aaaa(qname, h.ttl, ips)
		case dns.TypeSRV:
			destinations := lazyMustFetchDestinations()
			answers = srv(qname, h.ttl, destinations.ports)
		default: //All other record types
			//Be careful, we should not fallthough if it would create an infinite loop
			log.Debug("Ignoring a recordType ", state.QType, " request for ", qname)
			retInt, retErr = plugin.NextOrFailure(h.Name(), h.Next, ctx, w, r)
			return
		}
		m := new(dns.Msg)
		m.SetReply(r)
		m.Authoritative = true
		m.Answer = answers

		w.WriteMsg(m)
		log.Debug("drax-dns done setting authoritatian reply")
		retInt, retErr = dns.RcodeSuccess, nil
		return
	}()
	return
}

// Name implements the plugin.Handle interface.
func (h DraxDNS) Name() string { return "draxdns" }

// a takes a slice of net.IPs and returns a slice of A RRs.
func a(zone string, ttl uint32, ips []net.IP) []dns.RR {
	answers := make([]dns.RR, len(ips))
	for i, ip := range ips {
		r := new(dns.A)
		r.Hdr = dns.RR_Header{Name: zone, Rrtype: dns.TypeA, Class: dns.ClassINET, Ttl: ttl}
		r.A = ip
		answers[i] = r
	}
	return answers
}

// aaaa takes a slice of net.IPs and returns a slice of AAAA RRs.
func aaaa(zone string, ttl uint32, ips []net.IP) []dns.RR {
	answers := make([]dns.RR, len(ips))
	for i, ip := range ips {
		r := new(dns.AAAA)
		r.Hdr = dns.RR_Header{Name: zone, Rrtype: dns.TypeAAAA, Class: dns.ClassINET, Ttl: ttl}
		r.AAAA = ip
		answers[i] = r
	}
	return answers
}

// a takes a slice of net.IPs and returns a slice of SRV RRs.
func srv(zone string, ttl uint32, ports []uint16) []dns.RR {
	answers := make([]dns.RR, len(ports))
	for i, port := range ports {
		r := new(dns.SRV)
		r.Hdr = dns.RR_Header{Name: zone, Rrtype: dns.TypeSRV, Class: dns.ClassINET, Ttl: ttl}
		r.Port = port
		r.Target = zone
		answers[i] = r
	}
	return answers
}
