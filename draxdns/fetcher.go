//Written by Anirudh Katoch (katoch.anirudh@gmail.com)

package draxdns

import (
	"encoding/json"
	"net"
	"net/http"
	"net/url"
	"strings"
)

//DraxAPI is an object encapsulating an API request
type DraxAPI struct {
	path   string
	method string
	args   map[string]string
}

//ProvisionNodesForServer provisions a node for the server (if required) and returns its IP and port
func ProvisionNodesForServer(serverName string) DraxAPI {
	return DraxAPI{
		path:   "/draxapi/v1/ProvisionNodesForServer",
		method: "GET",
		args: map[string]string{
			"serverName": serverName,
		},
	}
}

//Destinations represents the information returned by the HTTP lookup
//The client can connect to any of the ip/port combinations to access the destination
type Destinations struct {
	ipv4  []net.IP
	ipv6  []net.IP
	ports []uint16
}

//Gets ipv4 and ipv6 addresses, as well as port, for a given host string
func (fetcher *Fetcher) fetchDestinations(zone string) (Destinations, error) {
	zone = strings.ToLower(zone) //e.g. 12414.drax.io
	destinations := Destinations{}
	err := fetcher.callAPI(ProvisionNodesForServer(zone), &destinations)
	return destinations, err
}

//Fetcher provides IPs and Ports for a given zone string
type Fetcher struct {
	//host string (e.g. http://selfservice.consul:8080) to that hosts the HTTP server which provisions and returns IPs
	destinationsProviderAPIPrefix *url.URL
}

func (fetcher *Fetcher) callAPI(apiObject DraxAPI, output interface{}) error {
	apiPath := apiObject.path
	query := apiObject.args
	urlObj := url.URL(*fetcher.destinationsProviderAPIPrefix)

	urlObj.Path += apiPath

	// Query params
	params := url.Values{}
	for k, v := range query {
		params.Add(k, v)
	}
	urlObj.RawQuery = params.Encode()

	log.Info("drax-dns making HTTP request ", urlObj.String())
	req, err := http.Get(urlObj.String())
	if err != nil {
		return err
	}
	defer req.Body.Close()

	return json.NewDecoder(req.Body).Decode(output)
}
