package draxdns

import (
	"net/url"
	"strconv"

	"github.com/coredns/coredns/core/dnsserver"
	"github.com/coredns/coredns/plugin"
	clog "github.com/coredns/coredns/plugin/pkg/log"

	"github.com/caddyserver/caddy"
)

var log = clog.NewWithPlugin("draxdns")

func init() { plugin.Register("draxdns", setup) }

func setup(c *caddy.Controller) error {
	args := c.RemainingArgs()
	destinationsProviderAPIPrefix, err := url.Parse(args[1])
	if err != nil {
		log.Error("Error parsing draxdns arg[0]: ", args[1], " ", err, " expected https://localhost:8080")
	}
	ttl, err := strconv.ParseUint(args[2], 10, 16)
	if err != nil {
		log.Error("Error parsing draxdns arg[1]: ", args[2], " ", err, " expected TTL (uint32)")
	}
	dnsserver.GetConfig(c).AddPlugin(func(next plugin.Handler) plugin.Handler {
		draxdns := DraxDNS{
			DraxDNSOptions: DraxDNSOptions{ttl: uint32(ttl)},
			Fetcher: &Fetcher{
				destinationsProviderAPIPrefix: destinationsProviderAPIPrefix,
			},
		}
		draxdns.Next = next
		return draxdns
	})

	return nil
}
