# drax-dns

type lazy struct {
	once       *sync.Once
	expression func(chan interface{})
	returns    []interface{}
}

func initLazy(expression func(chan interface{})) lazy {
	return lazy{expression: expression}
}

func (l *lazy) eval() interface{} {
	l.once.Do(func() {
		ret := make(chan interface{})
		l.expression(ret)
		for
	})
	return l.result
}

func pack(args ...interface{}) []interface{} {
	return args
}
